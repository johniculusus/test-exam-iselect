package com.iselect.util;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtility
{
	private static final String EMPTY_STRING = "";
	private static final String WHITE_SPACE = " ";

	public static boolean hasFoundText(String stringToSearch,String faceCoordinate) {
//        Pattern p = Pattern.compile("(?i)(?<= |^)" + commandlist + "(?= |$)");   // the pattern to search for
		Pattern p = Pattern.compile("(?i)" + faceCoordinate);   // the pattern to search for
		
        Matcher m = p.matcher(stringToSearch);
        
        return m.find();
	}
	
	public static boolean isEmpty(String s){
		return !isNotEmpty(s);
	}

	public static boolean isNotEmpty(String s){
		return (s != null && !EMPTY_STRING.equalsIgnoreCase(s) && s.length() > 0); /* overkill */
	}

	public static String removeSpaces(String s){
		return removeCharacters(s, WHITE_SPACE);
	}

	public static String removeCharacter(String s, char c){
		return removeCharacters(s, Character.toString(c));
	}

	public static String removeCharacters(String s, String strToRemove){
		if (isNotEmpty(s) && isNotEmpty(strToRemove)){
			StringTokenizer st = new StringTokenizer(s, strToRemove, false);
			s = EMPTY_STRING;
			while (st.hasMoreElements())
				s += st.nextElement();
		}

		return s;
	}

	public static String replaceLastCharacter(String s, String lastChar, String replacement){
		if (isNotEmpty(s) && isNotEmpty(lastChar) && replacement != null) /* replacement can be white space */
			if (s.lastIndexOf(lastChar) == s.length() - 1)
				return s.substring(0, s.length() - 1) + replacement;

		return s;
	}

	public static String replaceLastCharacter(String str, char replacement){
		return replaceLastCharacter(str, Character.toString(replacement));
	}

	public static String replaceLastCharacter(String s, String replacement){
		if (isNotEmpty(s) && replacement != null) /* replacement can be white space */
			return s.substring(0, s.length() - 1) + replacement;

		return s;
	}
}
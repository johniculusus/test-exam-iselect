import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.iselect.util.StringUtility;

public class MoveTheRobotTest {
	final static Logger LOGGER = Logger.getLogger(MoveTheRobotTest.class);
	
	public static MoveTheRobotTest wew = new MoveTheRobotTest();
	
	String robotCurrentFaceCoordinate = "";
	
	public static String getInitCommand(String line) {
        return line.split(",", 3)[0].split(" ")[0].trim();
	}
	
	public static int getX(String line) {
		return Integer.parseInt(line.split(",", 3)[0].split(" ")[1].trim());
	}
	
	public static int getY(String line) {
        return Integer.parseInt(line.split(",", 3)[1].trim());
	}
	
	public static String getF(String line) {
		return line.split(",", 3)[2].trim();
	}

	public String getRobotCurrentFaceCoordinate() {
		return robotCurrentFaceCoordinate;
	}

	public void setRobotCurrentFaceCoordinate(String robotCurrentFaceCoordinate) {
		this.robotCurrentFaceCoordinate = robotCurrentFaceCoordinate;
	}

	public static void main(String[] args) {
//		Point robotLocation = new Point();
		
		RobotCoordinates robotLocation = new RobotCoordinates();
		
        InputStream is = null;
        BufferedReader br = null;
        String line = null;
        String command = null;
        Properties prop = new Properties();
        
        try{
        	prop.load(new FileInputStream("conf/input.properties"));
        	is = System.in;
            br = new BufferedReader(new InputStreamReader(is));
            
            System.out.println("Please enter PLACE command for the robot to move on. x=x-axis; y=y-axis; f=facing coordinate (N,W,E,S)");
            System.out.println("Considering x/y coordinate of 0,0 - South West most corner, without any (-)/Negative axis coordinates. And maximum of 5x5 square dimension.\n");
            
//            System.out.println(prop.getProperty("face.coordinates"));
            
            outerloop:
            while((line = br.readLine()) != null) {
            	
                int x = getX(line);
                int y = getY(line);
                String f = getF(line);
                
                LOGGER.debug("Initialized first command.");
                LOGGER.debug("X coordinate\t\t= "+x);
                LOGGER.debug("Y coordinate\t\t= "+y);
                LOGGER.debug("F cardinal\t\t= "+f);
                LOGGER.debug("-------------------");
                
                
                if (line.equalsIgnoreCase("quit") || line.equalsIgnoreCase("report")) {
                    System.out.println("X coordinate\t\t= "+robotLocation.getxCoordinate());
                    System.out.println("Y coordinate\t\t= "+robotLocation.getyCoordinate());
                    System.out.println("F cardinal\t\t= "+robotLocation.getfCoordinate());
                    LOGGER.info("X coordinate\t\t= "+robotLocation.getxCoordinate());
                    LOGGER.info("Y coordinate\t\t= "+robotLocation.getyCoordinate());
                    LOGGER.info("F cardinal\t\t= "+robotLocation.getfCoordinate());
                    LOGGER.debug("REPORT initialized. Ending application...");
                    LOGGER.debug("-------------------");
                	break;
                }
                
                boolean validateFaceCoordinate = StringUtils.isNotBlank(f) && StringUtility.hasFoundText(prop.getProperty("face.coordinates"), f);
                
                if(getInitCommand(line).equalsIgnoreCase("PLACE") && x>=0 && y>=0 && validateFaceCoordinate) {
                	
                	robotLocation.setxCoordinate(x);
                    robotLocation.setyCoordinate(y);
                    robotLocation.setfCoordinate(f);

                    LOGGER.debug("VALID Initialized first command.");
                    LOGGER.debug("ENTERING SECONDARY COMMANDS...");
                    
                	while((command = br.readLine()) != null) {
                		if (command.equalsIgnoreCase("quit") || command.equalsIgnoreCase("report")) {
                          System.out.println("X coordinate\t\t= "+robotLocation.getxCoordinate());
                          System.out.println("Y coordinate\t\t= "+robotLocation.getyCoordinate());
                          System.out.println("F cardinal\t\t= "+robotLocation.getfCoordinate());
                          LOGGER.info("X coordinate\t\t= "+robotLocation.getxCoordinate());
                          LOGGER.info("Y coordinate\t\t= "+robotLocation.getyCoordinate());
                          LOGGER.info("F cardinal\t\t= "+robotLocation.getfCoordinate());
                          LOGGER.debug("REPORT initialized. Ending application...");
                          LOGGER.debug("-------------------");
                        	break outerloop;
                        }
                		
                		//revalidate if reset place triggered
                		if(getInitCommand(command).equalsIgnoreCase("PLACE")) {
                			
                			if(getX(command) >= 0 && getY(command) >= 0 && StringUtils.isNotBlank(f) && StringUtility.hasFoundText(prop.getProperty("face.coordinates"), getF(command))) {
                				robotLocation.setxCoordinate(getX(command));
                                robotLocation.setyCoordinate(getY(command));
                                robotLocation.setfCoordinate(getF(command));
                                
                                LOGGER.debug("X coordinate\t\t= "+robotLocation.getxCoordinate());
                                LOGGER.debug("Y coordinate\t\t= "+robotLocation.getyCoordinate());
                                LOGGER.debug("F cardinal\t\t= "+robotLocation.getfCoordinate());
                                LOGGER.debug("RESET ON PLACE COMMAND TRIGGERED...");
                                LOGGER.debug("-------------------");
                    			continue;
                			}
                			
                			continue;
                		}
                		
                		if(StringUtils.containsIgnoreCase(prop.getProperty("slots.command"), command)) {
                            LOGGER.debug("VALID SECONDARY COMMAND : " + command);
                            LOGGER.debug("-------------------");
                			
                			if(StringUtils.equalsAnyIgnoreCase(command, "LEFT") || StringUtils.equalsAnyIgnoreCase(command, "RIGHT") || StringUtils.equalsAnyIgnoreCase(command, "MOVE")) {
                				if(StringUtils.equalsAnyIgnoreCase(command, "LEFT")) {
                					if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "NORTH")) {
                						robotLocation.setfCoordinate("WEST");
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "EAST")) {
                						robotLocation.setfCoordinate("NORTH");
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "SOUTH")) {
                						robotLocation.setfCoordinate("EAST");
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "WEST")) {
                						robotLocation.setfCoordinate("SOUTH");
                					} else {
                						continue;
                					}

                						
                				}
                				if(StringUtils.equalsAnyIgnoreCase(command, "RIGHT")) {
                					if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "NORTH")) {
                						robotLocation.setfCoordinate("EAST");
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "EAST")) {
                						robotLocation.setfCoordinate("SOUTH");
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "SOUTH")) {
                						robotLocation.setfCoordinate("WEST");
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "WEST")) {
                						robotLocation.setfCoordinate("NORTH");
                					} else {
                						continue;
                					}
                				}
                				if(StringUtils.equalsAnyIgnoreCase(command, "MOVE")) {
                					int testX = robotLocation.getxCoordinate();
                					int testY = robotLocation.getyCoordinate();
                					String testF = robotLocation.getfCoordinate();
                					
                					if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "NORTH")) {
                						if((testX == 0) && (testY == 0)) {
                							robotLocation.setyCoordinate(testY + 1);
                						} else if((testX == 0) && (testY != 0 && (testY + 1) <= 5)) {
                							robotLocation.setyCoordinate(testY + 1);
                						} else if((testY == 0) && (testX != 0)) {
                							robotLocation.setyCoordinate(testY + 1);
                						} else if((testY != 0 && (testY + 1) <= 5) && (testX != 0 && (testX + 1) <= 5)) {
                							robotLocation.setyCoordinate(testY + 1);
                						} else {
                							continue;
                						}
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "EAST")) {
                						if((testX == 0) && (testY == 0)) {
                							robotLocation.setxCoordinate(testX + 1);
                						} else if((testX == 0) && (testY != 0)) {
                							robotLocation.setxCoordinate(testX + 1);
                						} else if((testY == 0) && (testX != 0 && (testX + 1) <= 5)) {
                							robotLocation.setxCoordinate(testX + 1);
                						} else if((testY != 0 && (testY + 1) <= 5) && (testX != 0 && (testX + 1) <= 5)) {
                							robotLocation.setxCoordinate(testX + 1);
                						} else {
                							continue;
                						}
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "SOUTH")) {
                						if((testX == 0) && (testY == 0)) {
                							continue;
                						} else if((testX == 0) && (testY != 0 && (testY - 1) <= 5)) {
                							robotLocation.setyCoordinate(testY - 1);
                						} else if((testY == 0) && (testX != 0)) {
                							continue;
                						} else if((testY != 0 && (testY - 1) <= 5) && (testX != 0 && (testX - 1) <= 5)) {
                							robotLocation.setyCoordinate(testY - 1);
                						} else {
                							continue;
                						}
                					} else if(StringUtils.equalsIgnoreCase(robotLocation.getfCoordinate(), "WEST")) {
                						if((testX == 0) && (testY == 0)) {
                							continue;
                						} else if((testX == 0) && (testY != 0)) {
                							continue;
                						} else if((testY == 0) && (testX != 0 && (testX - 1) <= 5)) {
                							robotLocation.setxCoordinate(testX - 1);
                						} else if((testY != 0 && (testY - 1) <= 5) && (testX != 0 && (testX - 1) <= 5)) {
                							robotLocation.setxCoordinate(testX - 1);
                						} else {
                							continue;
                						}
                					} else {
                						continue;
                					}
                				}
                				
                				LOGGER.debug("X coordinate\t\t= "+robotLocation.getxCoordinate());
                                LOGGER.debug("Y coordinate\t\t= "+robotLocation.getyCoordinate());
                                LOGGER.debug("F cardinal\t\t= "+robotLocation.getfCoordinate());
                                LOGGER.debug("ENDING SECONDARY COMMAND... CONTINUING...");
                                LOGGER.debug("-------------------");
                				
                				continue;
                			}
                		} else {
                			System.out.println("Input valid secondary command. RIGHT/LEFT/MOVE");
                			LOGGER.info("Input valid secondary command. RIGHT/LEFT/MOVE");
                			continue;
                		}
                	}
                } else {
                	System.out.println("INVALID COMMAND. enter something like: PLACE x,y,f");
                	LOGGER.info("INVALID COMMAND. enter something like: PLACE x,y,f");
                	continue;
                }
            }
        } catch(IOException ioe) {
        	System.out.println("Exception while reading input " + ioe);
        	LOGGER.error("Exception while reading input " + ioe);
        } catch(ArrayIndexOutOfBoundsException aioobe) {
        	System.out.println("Exception ArrayIndexOutOfBoundsException " + aioobe);
        	LOGGER.error("Exception ArrayIndexOutOfBoundsException " + aioobe);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            }
            catch (IOException ioe) {
                System.out.println("Error while closing stream: " + ioe);
                LOGGER.error("Error while closing stream: " + ioe);
            }
		}
	}
}
